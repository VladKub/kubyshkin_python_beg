def get_array(count: int):
    array = []
    for i in range(count):
        array.append(int(input("Введите элемент массива - ")))
    return array


def min_of_array(array: list):
    list = get_array
    for elem in list:
        min_a = elem if elem < elem else min_a
    return min_a

def max_of_array(array: list):
    list = get_array
    for elem in list:
        max_a = elem if elem < elem else max_a
    return max_a


def array_sum(array:list):
    list = get_array
    sum = 0
    for i in array:
        sum +=i
    return sum


def factorial(elem):
    fact = 1
    for elem in range(2, elem+1):
        fact *= elem
    return fact


def add_factorials_to_set(array: list) -> set:
    facts = set()
    for elem in array:
        facts.add(factorial(elem))
    return facts

if __name__ == '__main__':

    #Получение списка
    array = get_array(int(input("Введите кол-во элементов - ")))
    # Получение суммы
    print(array)
    sum1 = array_sum(array)
    print(f"Сумма элементов списка = {sum1}")
    #Мин
    min_a = min_of_array(array)
    print(f"Мин ="+ min_a)
    #Макс
    max_a = max_of_array(array)
    print(f"Макс ="+ max_a)
    #Факториалы
    facts = add_factorials_to_set(array)
    print(f"Множество из факториалов списка = {facts}")

