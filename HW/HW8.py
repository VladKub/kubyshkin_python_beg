def count_of_upper_letter(string:str):
    count = 0
    for word in string:
        if 'A' <= word <= 'Z':
            count += 1
    return count


if __name__ == '__main__':
    print(count_of_upper_letter(input('Введите текст на англ языке через пробелы - ')))
