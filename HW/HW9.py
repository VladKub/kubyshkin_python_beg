class Vector2d:

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector2d(x=(self.x + other.x), y=(self.y + other.y))

    def add(self, other):
        self.x += other.x
        self.y += other.y

    def __sub__(self, other):
        return Vector2d(x=(self.x - other.x), y=(self.y - other.y))

    def sub(self, other):
        self.x -= other.x
        self.y -= other.y

    def __mul__(self, other):
        return Vector2d(x=(self.x * other.x), y=(self.y * other.y))

    def mul(self, other):
        self.x *= other.x
        self.y *= other.y

    def __str__(self):
        return "Vector(x = {}, y = {}".format(self.x, self.y)

    def __len__(self):
        return abs(self.x - self.y)

    def scalar(self, other):
        return self.__len__() * len(other)

    def __eq__(self, other):
        return (self.x == other.x) and (self.y == other.y)
