class RationalFraction:

    def __init__(self, numerator: int, denominator: int):
        if denominator < 1:
            raise Exception
        self.numerator = numerator
        self.denominator = denominator

    def __add__(self, other):
        ans = RationalFraction(denominator=(self.denominator * other.denominator)),
        numerator=((self.numerator*other.denominator)+(other.numerator*self.denominator))
        return ans

    def __iadd__(self, other):
        self.numerator = (self.numerator * other.denominator) + (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator

    def __sub__(self, other):
        ans = RationalFraction(denominator=(self.denominator * other.denominator)),
        numerator=((self.numerator*other.denominator)-(other.numerator*self.denominator))

    def __isub__(self, other):
        self.numerator = (self.numerator * other.denominator) - (other.numerator * self.denominator)
        self.denominator = self.denominator * other.denominator

    def __eq__(self, other):
        return (self.numerator == other.numerator) and (self.denominator == other.denominator)

    def __str__(self):
        return "{}/{}".format(self.numerator, self.denominator)
